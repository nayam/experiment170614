package jp.gr.java_conf.ny

import java.util.concurrent.Executors
import java.util.stream.IntStream

data class Test(
        val tol: Double,
        val rsrP: Double,
        val rsrM: Double
)

data class Wallet(private val money: Int) {
    fun pay(money: Int) = Wallet(this.money - money)
}


fun factorial(n: Int) =
        (1..n - 1).map(Int::toString)
/*
tailrec fun factorial(n: Int, product: Int = 1): Int = when (n) {
    0 -> product
    else -> factorial(n - 1, product * n)
}*/

fun filterEven(numbers: List<Int>) =
        numbers.filter { it % 2 == 0 }

fun main(args: Array<String>) {
    /*
    val users = (1..100).map { User(0.1, 0.5, 0.2, 1.0) }

    val list = listOf(0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0)

    listOf(1, 2, 3, 4).fold(1) { sum, i -> sum + i }

    list.forEach {
        risk ->
        val test = users.map {
            it to TOL(it)
        }.map { (user, tol) ->
            val t = tol.tol(risk)

            val rsrP = user.requirementSatisfactionRatePrivacy(risk, t)
            val rsrM = user.requirementSatisfactionRateMonitoring(risk, t)

            Test(t, rsrP, rsrM)
        }.filter { it.rsrM.isFinite() }
        val tolAve = test.map { it.tol }.average()
        val rsrPAve = test.map { it.rsrP }.average()
        val rsrMAve = test.map { it.rsrM }.average()
        println("$risk, $tolAve, $rsrPAve, $rsrMAve")
    }



    list.forEach {
        risk ->
        val test = users.map {
            user ->

            val t = 0.8

            val rsrP = user.requirementSatisfactionRatePrivacy(risk, t)
            val rsrM = user.requirementSatisfactionRateMonitoring(risk, t)

            Test(t, rsrP, rsrM)
        }
        val tolAve = test.map { it.tol }.average()
        val rsrPAve = test.map { it.rsrP }.average()
        val rsrMAve = test.map { it.rsrM }.average()
        println("$risk, $tolAve, $rsrPAve, $rsrMAve")
    }*/
}

