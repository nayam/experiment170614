package jp.gr.java_conf.ny;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Exam {
    public static void main(final String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final int students = Integer.parseInt(scanner.nextLine());

        final List<AbstractStudent> studentList = IntStream.range(0, students)
                .mapToObj(i -> scanner.nextLine())
                .map(s -> {
                    final String[] list = s.split(" ");
                    switch (list[0]) {
                        case "l":
                            return new LiberalArtsStudent(
                                    Integer.parseInt(list[1]),
                                    Integer.parseInt(list[2]),
                                    Integer.parseInt(list[3]),
                                    Integer.parseInt(list[4]),
                                    Integer.parseInt(list[5])
                            );
                        case "s":
                            return new ScienceStudent(
                                    Integer.parseInt(list[1]),
                                    Integer.parseInt(list[2]),
                                    Integer.parseInt(list[3]),
                                    Integer.parseInt(list[4]),
                                    Integer.parseInt(list[5])
                            );
                        default:
                            throw new IllegalArgumentException();
                    }
                })
                .collect(Collectors.toList());

        System.out.println(studentList.stream().filter(AbstractStudent::isPassed).count());
    }


}

final class LiberalArtsStudent extends AbstractStudent {
    LiberalArtsStudent(final int englishScore, final int mathematicsScore, final int scienceScore,
                       final int japaneseScore, final int historyScore) {
        super(englishScore, mathematicsScore, scienceScore, japaneseScore, historyScore);
    }

    @Override
    public boolean isPassed() {
        if (englishScore + mathematicsScore + scienceScore + japaneseScore + historyScore < 350) {
            return false;
        }

        if (japaneseScore + historyScore < 160) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder("LiberalArtsStudent(");
        builder.append(englishScore).append(',');
        builder.append(mathematicsScore).append(',');
        builder.append(scienceScore).append(',');
        builder.append(japaneseScore).append(',');
        builder.append(historyScore).append(',');
        builder.append(isPassed()).append(')');
        return builder.toString();
    }
}

final class ScienceStudent extends AbstractStudent {
    ScienceStudent(final int englishScore, final int mathematicsScore, final int scienceScore,
                   final int japaneseScore, final int historyScore) {
        super(englishScore, mathematicsScore, scienceScore, japaneseScore, historyScore);
    }

    @Override
    public boolean isPassed() {
        if (englishScore + mathematicsScore + scienceScore + japaneseScore + historyScore < 350) {
            return false;
        }

        if (mathematicsScore + scienceScore < 160) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder("ScienceStudent(");
        builder.append(englishScore).append(',');
        builder.append(mathematicsScore).append(',');
        builder.append(scienceScore).append(',');
        builder.append(japaneseScore).append(',');
        builder.append(historyScore).append(',');
        builder.append(isPassed()).append(')');
        return builder.toString();
    }

}

abstract class AbstractStudent {
    final int englishScore;
    final int mathematicsScore;
    final int scienceScore;
    final int japaneseScore;
    final int historyScore;

    AbstractStudent(final int englishScore, final int mathematicsScore, final int scienceScore,
                    final int japaneseScore, final int historyScore) {
        this.englishScore = englishScore;
        this.mathematicsScore = mathematicsScore;
        this.scienceScore = scienceScore;
        this.japaneseScore = japaneseScore;
        this.historyScore = historyScore;
    }

    abstract boolean isPassed();
}