package jp.gr.java_conf.ny


fun main(args: Array<String>) {

}

data class Card(
        val rank: Int,
        val suit: Char
)
fun print(numbers: List<Int>) =
        // 副作用と併せて使う
        // 代入操作などの不変性を破壊する副作用に使うのは避ける
        // メインは結果の出力
        numbers.forEach(System.out::println)

data class Deck(val list: List<Card>){
    fun shuffle(){
        //return list.map {  }
    }
}