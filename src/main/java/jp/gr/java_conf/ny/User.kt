package jp.gr.java_conf.ny

import java.security.SecureRandom

private val random = SecureRandom.getInstanceStrong()



// floating point の key は良くないんだけど 今回は目をつむろう
fun nextMonitoring(min: Double = 0.0, max: Double = 1.0): Map<Double, Double> {

    val diff = max - min

    return listOf(
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff
    ).sorted().withIndex().associate { it.index / 10.0 to it.value }
}

fun nextPrivacy(min: Double = 0.0, max: Double = 1.0): Map<Double, Double> {

    val diff = max - min

    return listOf(
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff,
            min + random.nextDouble() * diff
    ).sorted().reversed().withIndex().associate { it.index / 10.0 to it.value }
}


data class User(
        val iPrivacy: Map<Double, Double> = nextPrivacy(),
        val iMonitoring: Map<Double, Double> = nextMonitoring()) {

    constructor(privacyMin: Double, privacyMax: Double, monitoringMax: Double, monitoringMin: Double)
            : this(iPrivacy = nextPrivacy(privacyMin, privacyMax), iMonitoring = nextMonitoring(monitoringMin, monitoringMax))

    fun requirementSatisfactionRatePrivacy(risk: Double, tol: Double): Double {
        return tol / iPrivacy[risk]!!
    }


    fun requirementSatisfactionRateMonitoring(risk: Double, tol: Double): Double {
        return tol / iMonitoring[risk]!!
    }
}