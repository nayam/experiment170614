package jp.gr.java_conf.ny;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class Main {

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final int height;
        final int width;
        {
            final String[] array = scanner.nextLine().split(" ");
            height = Integer.parseInt(array[0]);
            width = Integer.parseInt(array[1]);
        }

        final Board board = new Board(IntStream.range(0, height)
                .mapToObj(i ->
                        scanner.nextLine()
                                .chars()
                                .mapToObj(ch -> Dice.Pip.of((char) ch))
                                .collect(Collectors.toList()))
                .collect(Collectors.toList()));

        System.out.println(board.search().getAsInt());
    }
}

final class CostTable {
    private final Map<List<Integer>, Map<Dice.State, Integer>> map = new HashMap<>();

    // TODO Dice を key にしたら stackoverflow あとで確認
    void put(final Dice dice, final int cost) {
        map.computeIfAbsent(Stream.of(dice.y, dice.x).collect(Collectors.toList()), i -> new HashMap<>()).put(dice.state, cost);
    }

    int getOrDefault(final Dice dice, final int defaultValue) {
        return map.getOrDefault(Stream.of(dice.y, dice.x).collect(Collectors.toList()), Collections.emptyMap()).getOrDefault(dice.state, defaultValue);
    }
}

final class Board {
    private final List<List<Dice.Pip>> board;
    private final int height;
    private final int width;

    Board(final List<List<Dice.Pip>> board) {
        this.height = board.size();
        this.width = height == 0 ? 0 : board.get(0).size();
        this.board = board;
    }

    OptionalInt search() {
        final List<Integer> result = new ArrayList<>();
        search(new CostTable(), new Dice(0, 0, new Dice.State(Dice.Pip.ONE, Dice.Pip.TWO, Dice.Pip.THREE)), 0, result);

        return result.stream().mapToInt(Integer::intValue).min();
    }

    void search(final CostTable costTable, final Dice dice, final int cost, final List<Integer> goals) {
        if (cost >= goals.stream().mapToInt(Integer::intValue).min().orElse(Integer.MAX_VALUE)) {
            return;
        }

        if (dice.x == width - 1 && dice.y == height - 1) {
            goals.add(cost);
            return;
        }

        if (dice.y != 0) {
            final Dice nextDice = dice.rollUp();
            final int nextCost = nextDice.state.front.cost(board.get(nextDice.y).get(nextDice.x)) + cost;

            if (nextCost < costTable.getOrDefault(nextDice, Integer.MAX_VALUE)) {
                costTable.put(nextDice, nextCost);
                search(costTable, nextDice, nextCost, goals);
            }
        }

        if (dice.y != height - 1) {
            final Dice nextDice = dice.rollDown();
            final int nextCost = nextDice.state.front.cost(board.get(nextDice.y).get(nextDice.x)) + cost;

            if (nextCost < costTable.getOrDefault(nextDice, Integer.MAX_VALUE)) {
                costTable.put(nextDice, nextCost);
                search(costTable, nextDice, nextCost, goals);
            }
        }

        if (dice.x != 0) {
            final Dice nextDice = dice.rollLeft();
            final int nextCost = nextDice.state.front.cost(board.get(nextDice.y).get(nextDice.x)) + cost;

            if (nextCost < costTable.getOrDefault(nextDice, Integer.MAX_VALUE)) {
                costTable.put(nextDice, nextCost);
                search(costTable, nextDice, nextCost, goals);
            }
        }

        if (dice.x != width - 1) {
            final Dice nextDice = dice.rollRight();
            final int nextCost = nextDice.state.front.cost(board.get(nextDice.y).get(nextDice.x)) + cost;

            if (nextCost < costTable.getOrDefault(nextDice, Integer.MAX_VALUE)) {
                costTable.put(nextDice, nextCost);
                search(costTable, nextDice, nextCost, goals);
            }
        }
    }
}


final class Dice {
    final int x;
    final int y;
    final State state;

    Dice(final int x, final int y, final State state) {
        this.x = x;
        this.y = y;
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Dice that = (Dice) o;

        if (x != that.x) return false;
        if (y != that.y) return false;
        return state == that.state;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + x;
        result = 31 * result + y;
        result = 31 * result + state.hashCode();
        return result;
    }

    enum Pip {
        ONE(1, '1') {
            @Override
            Pip back() {
                return SIX;
            }
        }, TWO(2, '2') {
            @Override
            Pip back() {
                return FIVE;
            }
        }, THREE(3, '3') {
            @Override
            Pip back() {
                return FOUR;
            }
        }, FOUR(4, '4') {
            @Override
            Pip back() {
                return THREE;
            }
        }, FIVE(5, '5') {
            @Override
            Pip back() {
                return TWO;
            }
        }, SIX(6, '6') {
            @Override
            Pip back() {
                return ONE;
            }
        };

        abstract Pip back();

        private final int number;
        private final char ch;

        Pip(final int number, final char ch) {
            this.number = number;
            this.ch = ch;
        }

        private static final Map<Character, Pip> map = Stream.of(values())
                .collect(Collectors.toMap(pip -> pip.ch, Function.identity()));

        static Pip of(char ch) {
            return map.get(ch);
        }

        final int cost(final Pip that) {
            return Math.abs(this.number - that.number);
        }
    }

    static final class State {
        final Pip front;
        final Pip back;
        final Pip up;
        final Pip down;
        final Pip left;
        final Pip right;

        State(final Pip front, final Pip up, final Pip right) {
            this.front = front;
            this.back = front.back();
            this.up = up;
            this.down = up.back();
            this.right = right;
            this.left = right.back();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            final State that = (State) o;

            if (front != that.front) return false;
            if (up != that.up) return false;
            return right == that.right;
        }

        @Override
        public int hashCode() {
            int result = 17;
            result = 31 * result + front.hashCode();
            result = 31 * result + up.hashCode();
            result = 31 * result + right.hashCode();
            return result;
        }
    }

    final Dice rollRight() {
        return new Dice(x + 1, y, new State(state.left, state.up, state.front));
    }

    final Dice rollLeft() {
        return new Dice(x - 1, y, new State(state.right, state.up, state.back));
    }

    final Dice rollUp() {
        return new Dice(x, y - 1, new State(state.down, state.front, state.right));
    }

    final Dice rollDown() {
        return new Dice(x, y + 1, new State(state.up, state.back, state.right));
    }

}