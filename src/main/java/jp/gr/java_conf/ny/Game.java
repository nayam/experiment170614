package jp.gr.java_conf.ny;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class Game {
    public static void main(final String[] args) {
        final Rulebook rulebook = Rulebook.INSTANCE;
        final Collection<Player> players = Stream.of(
                new Computer("Computer1"),
                new Debugger("Debugger2", Hand.SCISSORS),
                new Debugger("Debugger3", Hand.PAPER)
        ).collect(Collectors.collectingAndThen(
                Collectors.toList(),
                Collections::unmodifiableCollection
        ));
        final int iterate = 10000;



        IntStream.range(0, iterate)
                .mapToObj(i -> rulebook.play(players))
                .peek(c -> System.out.println("Winners : " + c))
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .forEach((player, count) -> System.out.println(player + " : " + (count * 100d / iterate) + "%"));
    }
}

interface Player {
    Hand nextHand();
}

class Human implements Player {
    private final String name;

    Human(final String name) {
        this.name = name;
    }

    @Override
    public Hand nextHand() {
        System.out.println("0 : Rock, 1 : Paper, 2 : Scissors");
        final int number = new Scanner(System.in).nextInt();
        final Hand hand = Hand.toHand(number);
        System.out.println(name + " : " + hand);
        return hand;
    }

    @Override
    public String toString() {
        return name;
    }
}

class Debugger implements Player {
    private final String name;
    private final Hand hand;

    Debugger(final String name, final Hand hand) {
        this.name = name;
        this.hand = hand;
    }

    @Override
    public Hand nextHand() {
        System.out.println(name + " : " + hand);
        return hand;
    }

    @Override
    public String toString() {
        return name;
    }
}

class Computer implements Player {
    private final String name;
    private final Random random = ThreadLocalRandom.current();

    Computer(final String name) {
        this.name = name;
    }

    @Override
    public Hand nextHand() {
        final int number = random.nextInt(3);
        final Hand hand = Hand.toHand(number);
        System.out.println(name + " : " + hand);
        return hand;
    }

    @Override
    public String toString() {
        return name;
    }
}

enum Rulebook {
    INSTANCE {
        @Override
        public Collection<Player> play(Collection<Player> players) {
            final Map<Player, Hand> map = players.stream()
                    .collect(Collectors.toMap(Function.identity(), Player::nextHand));
            return map.entrySet().stream()
                    .filter(entry -> map.values().stream().anyMatch(entry.getValue()::isWin))
                    .filter(entry -> map.values().stream().allMatch(entry.getValue()::isNotLose))
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toSet());
        }
    }, DEBUG {
        @Override
        public Collection<Player> play(Collection<Player> players) {
            return players;
        }
    };

    abstract Collection<Player> play(Collection<Player> players);
}

enum Hand {
    ROCK(0) {
        @Override
        boolean isNotLose(final Hand hand) {
            return hand != Hand.PAPER;
        }

        @Override
        boolean isWin(final Hand hand) {
            return hand == Hand.SCISSORS;
        }
    },
    PAPER(1) {
        @Override
        boolean isNotLose(final Hand hand) {
            return hand != Hand.SCISSORS;
        }

        @Override
        boolean isWin(final Hand hand) {
            return hand == Hand.ROCK;
        }
    },
    SCISSORS(2) {
        @Override
        boolean isNotLose(final Hand hand) {
            return hand != Hand.ROCK;
        }

        @Override
        boolean isWin(final Hand hand) {
            return hand == Hand.PAPER;
        }
    };

    private final int number;

    Hand(final int number) {
        this.number = number;
    }

    private static final Map<Integer, Hand> reversed =
            Arrays.stream(values()).collect(
                    Collectors.collectingAndThen(
                            Collectors.toMap(hand -> hand.number, Function.identity()),
                            Collections::<Integer, Hand>unmodifiableMap));

    static Hand toHand(final int number) {
        return reversed.get(number);
    }

    abstract boolean isNotLose(final Hand hand);

    abstract boolean isWin(final Hand hand);
}