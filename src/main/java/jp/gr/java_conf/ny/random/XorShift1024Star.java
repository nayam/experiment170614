package jp.gr.java_conf.ny.random;

/**
 * Created by Iruka on 2017/07/06.
 */
public final class XorShift1024Star implements MyRandom {
    private final long s00;
    // private final long s01;
    private final long s02;
    private final long s03;
    private final long s04;
    private final long s05;
    private final long s06;
    private final long s07;
    private final long s08;
    private final long s09;
    private final long s10;
    private final long s11;
    private final long s12;
    private final long s13;
    private final long s14;
    private final long s15;

    public XorShift1024Star(final long s00,
                            final long s01,
                            final long s02,
                            final long s03,
                            final long s04,
                            final long s05,
                            final long s06,
                            final long s07,
                            final long s08,
                            final long s09,
                            final long s10,
                            final long s11,
                            final long s12,
                            final long s13,
                            final long s14,
                            final long s15) {
        this.s00 = s00;
        // this.s01 = s01;
        this.s02 = s02;
        this.s03 = s03;
        this.s04 = s04;
        this.s05 = s05;
        this.s06 = s06;
        this.s07 = s07;
        this.s08 = s08;
        this.s09 = s09;
        this.s10 = s10;
        this.s11 = s11;
        this.s12 = s12;
        this.s13 = s13;
        this.s14 = s14;
        this.s15 = s15;

        final long a = s01 ^ (s01 << 31);
        this.d = a ^ s00 ^ (a >>> 11) ^ (s00 >>> 30);
    }

    private final long d;

    @Override
    public MyRandom nextSeed() {
        return new XorShift1024Star(d, s02, s03, s04, s05, s06, s07, s08, s09, s10, s11, s12, s13, s14, s15, s00);
    }

    @Override
    public long nextValue() {
        return d * 1181783497276652981L;
    }
}
