package jp.gr.java_conf.ny.random;

public interface MyRandom {
    MyRandom nextSeed();
    long nextValue();
}
