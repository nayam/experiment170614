package jp.gr.java_conf.ny.random

import java.util.*

fun main(args: Array<String>) {
    while (true) {
        val human1: Player = Human("人1")
        val human2: Player = Human("人2")
        val human3: Player = Human("人3")

        val winner = RuleBook.whoWin(
                human1 to human1.hand(),
                human2 to human2.hand(),
                human3 to human3.hand()
        )
        println("勝者は $winner です")
    }
}

class Human(val name: String) : Player {
    override fun hand(): Hand {
        println("0 : Rock, 1 : Paper, 2 : Scissors")
        val number = Scanner(System.`in`).nextInt()
        val hand = Hand.toHand(number)
        println("$this は $hand を出しました")
        return hand
    }

    override fun toString(): String {
        return name
    }
}

class Debugger(val hand: Hand) : Player {
    override fun hand(): Hand {
        println("$this は $hand を出しました")
        return hand
    }

    override fun toString(): String {
        return hand.toString()
    }
}

class Computer(val name: String) : Player {
    val random = Random()

    override fun hand(): Hand {
        val number = random.nextInt(3) + 1
        val hand = Hand.toHand(number)
        println("$this は $hand を出しました")
        return hand
    }

    override fun toString(): String {
        return name
    }

}

object RuleBook {

    fun whoWin(vararg playerWithHand: Pair<Player, Hand>): List<Player> {
        val hands = playerWithHand.map { it.second }
        val players = playerWithHand.map { it.first }

        if (playerWithHand.map { it.second }.distinct().size == 1) {
            return emptyList()
        }

        if (playerWithHand.map { it.second }.distinct().size == 3) {
            return emptyList()
        }

        val winners = mutableListOf<Player>()

        (0..playerWithHand.size - 1).forEach {
            zibun ->
            if ((0..playerWithHand.size - 1).filter { it != zibun }
                    .all { aite -> hands[zibun].isNotLose(hands[aite]) }) {
                winners.add(players[zibun])
            }
        }

        return winners
    }
}

interface Player {
    fun hand(): Hand
}

enum class Hand(val number: Int) {
    Rock(0) {
        override fun isNotLose(hand: Hand) = hand != Paper
    },
    Paper(1) {
        override fun isNotLose(hand: Hand) = hand != Scissors
    },
    Scissors(2) {
        override fun isNotLose(hand: Hand) = hand != Rock
    };

    companion object {
        private val reverse by lazy { values().associate { it.number to it } }
        fun toHand(number: Int) = reverse[number]!!
    }

    abstract fun isNotLose(hand: Hand): Boolean
}