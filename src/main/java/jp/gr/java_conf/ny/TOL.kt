package jp.gr.java_conf.ny

data class TOL(val min: Double, val max: Double, val cy: Double, val cx: Double, val alpha: Double, val beta: Double = (1 - cx) * (cy - min) * alpha / cx / (max - cy)) {

    constructor(user: User) : this(
            min = (user.iPrivacy[0.0]!! + user.iMonitoring[0.0]!!) / 2,
            max = (user.iPrivacy[1.0]!! + user.iMonitoring[1.0]!!) / 2,
            cy = (user.iPrivacy[0.5]!! + user.iMonitoring[0.5]!!) / 2,
            cx = 0.5,
            alpha = 0.2
    )

    fun tol(risk: Double): Double {
        if (risk <= cx) {
            return min + (cy - min) * Math.pow((risk / cx), alpha)
        }
        return max - (max - cy) * Math.pow((1 - risk) / (1 - cx), beta)
    }
}